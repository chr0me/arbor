# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2015 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2012 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="http://llvm.org/git/llvm.git"
    require scm-git
else
    MY_PNV=${PNV}.src
    WORK=${WORKBASE}/${MY_PNV}
    SUFFIX=xz
    DOWNLOADS="https://llvm.org/releases/${PV}/${MY_PNV}.tar.${SUFFIX}"
fi

require alternatives
require cmake [ api=2 ]
# Although llvm does support python 3 basically nothing else in the LLVM stack does.
# Keep python 3 disabled for now so that we can use LLVM's installed version of lit
# for testing libc++{,abi} etc.
require setup-py [ import=distutils blacklist=3 has_bin=true multibuild=false ]

export_exlib_phases src_prepare src_configure src_compile src_test src_test_expensive src_install

SUMMARY="The LLVM Compiler Infrastructure"
HOMEPAGE="https://llvm.org/"

LICENCES="UoI-NCSA"

# See http://blog.llvm.org/2016/12/llvms-new-versioning-scheme.html for more info
# In "X.Y.Z", X is major release, Y is minor release, and Z is "patch" release
# Major version is the slot, except for any LLVM with major-release < 4

if ever is_scm && [[ $(ever major) == scm ]]; then
    SLOT="8"
elif ever is_scm ; then
    SLOT="${PV%-scm}"
elif ever at_least 6 ; then
    SLOT="$(ever major)"
else
    SLOT="0"
fi

MYOPTIONS="
    asserts [[ description = [ Enable assertions ] ]]
    libedit
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-lang/python:2.7 [[ description = [ build system always uses python2 ] ]]
        dev-python/setuptools[python_abis:*(-)?]
        sys-devel/flex
    build+run:
        sys-libs/zlib
        libedit? ( dev-libs/libedit )
        !sys-devel/binutils[=2.30] [[
            description = [ gold regressed in 2.30 ]
            resolution = upgrade-blocked-before
        ]]
    run:
        !dev-lang/llvm:0[<5.0.1-r1] [[
            description = [ Old, unslotted llvm not supported ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 6.0.0; then
    DEPENDENCIES+="
        build+run:
            dev-libs/libxml2:2.0
    "
fi

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/0001-Allow-CMAKE_BUILD_TYPE-None.patch
)

LLVM_PREFIX="/usr/$(exhost --target)/lib/llvm/${SLOT}"

llvm_src_prepare() {
    cmake_src_prepare

    # Fix the use of dot
    edo sed -e 's/@DOT@//g' -i docs/doxygen.cfg.in

    # These tests fail if gcc is not in path
    edo sed -e "s/bugpoint/\0 --gcc=${CC}/" -i test/BugPoint/*.ll
}

llvm_src_configure() {
    # TODO(compnerd) hidden inline visibility causes test tools to fail to build as a required
    # method is hidden; move the definition out of line, and export the interface
    local args=(
        -DLLVM_BINUTILS_INCDIR:STRING=/usr/$(exhost --is-native -q || echo "$(exhost --build)/$(exhost --build)/")$(exhost --target)/include/
        -DLLVM_DEFAULT_TARGET_TRIPLE:STRING=$(exhost --target)
        -DLLVM_ENABLE_ASSERTIONS:BOOL=$(option asserts TRUE FALSE)
        -DLLVM_INCLUDE_TESTS:BOOL=TRUE
        -DSUPPORTS_FVISIBILITY_INLINES_HIDDEN_FLAG:BOOL=NO

        # We always want to build LLVM's dylib, which is a shared library
        # that basically contains all of the split libraries. It's required
        # for most stuff that wants to link against LLVM. We can still build
        # LLVM statically by setting BUILD_SHARED_LIBS to FALSE below.
        -DLLVM_BUILD_LLVM_DYLIB:BOOL=TRUE
        # This only controls whether or not LLVM's utils link against the dylib.
        -DLLVM_LINK_LLVM_DYLIB:BOOL=TRUE

        # install LLVM to a slotted directory to prevent collisions with other llvm's
        -DCMAKE_INSTALL_PREFIX:STRING=${LLVM_PREFIX}
        -DCMAKE_INSTALL_MANDIR:STRING=${LLVM_PREFIX}/share/man

        # install utils (FileCheck, count, not) to `llvm-config --bindir`, so that
        # clang and others can use them
        -DLLVM_INSTALL_UTILS:BOOL=TRUE

        # Enable RTTI by default, Upstream and projects which need it (like mesa)
        # strongly recommend enabling it. Enabling it only costs a little disk space.
        -DLLVM_ENABLE_RTTI:BOOL=TRUE

        # Enable exception handling by default, it is basically free to build this
        # and some projects depend on it.
        -DLLVM_ENABLE_EH:BOOL=TRUE

        # Build tests in src_compile instead of src_test
        -DLLVM_BUILD_TESTS:BOOL=$(expecting_tests TRUE FALSE)

        -DLLVM_ENABLE_EXPENSIVE_CHECKS:BOOL=$(expecting_tests --expensive TRUE FALSE)

        -DLLVM_ENABLE_LIBEDIT:BOOL=$(option libedit TRUE FALSE)
    )

    if ever at_least 6.0.0; then
        args+=(
            -DLLVM_ENABLE_LIBXML2:BOOL=TRUE
        )
    fi

    ecmake "${args[@]}"
}

llvm_src_compile() {
    default

    edo pushd "${CMAKE_SOURCE}"/utils/lit
    setup-py_src_compile
    edo popd
}

llvm_src_test() {
    # TODO(Cogitri): As far as I can see LLVM doesn't offer a seperate
    # command to run non-expensive or expensive tests, so I resorted
    # to the following:
    expecting_tests --expensive || emake check
}

llvm_src_test_expensive() {
    emake check
}

llvm_src_install() {
    cmake_src_install

    # Make sure e.g. clang will not look for tools in the build directory
    edo sed \
        -e 's:^set(LLVM_TOOLS_BINARY_DIR .*)$:set(LLVM_TOOLS_BINARY_DIR '${LLVM_PREFIX}'/bin):' \
        -i "${IMAGE}${LLVM_PREFIX}"/lib/cmake/llvm/LLVMConfig.cmake

    # Symlink dynlibs to /usr/lib
    edo pushd "${IMAGE}${LLVM_PREFIX}/lib"
    for lib in $(ls libLLVM-*.so); do
        dosym "${LLVM_PREFIX}/lib/${lib}" "/usr/$(exhost --target)/lib/${lib}"
    done
    edo popd

    # Remove empty directory
    if ! ever is_scm ; then
        edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/llvm/MC/MCAnalysis
    fi

    if ever at_least 6.0.0; then
        nonfatal edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/llvm/BinaryFormat/WasmRelocs
    fi

    # Manage alternatives for llvm binaries and manpages
    alternatives=(
        "llvm" "${SLOT}" "${SLOT}"
    )

    edo pushd "${IMAGE}${LLVM_PREFIX}/bin"
    for bin in $(ls); do
        alternatives+=("/usr/$(exhost --target)/bin/${bin}" "${LLVM_PREFIX}/bin/${bin}")
    done
    edo popd

    ALTERNATIVES_llvm_DESCRIPTION="Alternatives for LLVM"
    alternatives_for "${alternatives[@]}"
}

