# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lib${PN} project=lib${PN} release=${PNV} suffix=tar.gz ]
require alternatives

SUMMARY="Filesystems in Userspace"
DESCRIPTION="
FUSE (Filesystem in Userspace) is a simple interface for userspace programs to export a virtual
filesystem to the Linux kernel. FUSE also aims to provide a secure method for non privileged users
to create and mount their own filesystem implementations.
"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run:
        sys-apps/util-linux[>=2.18]
        !sys-fs/fuse:0[<2.9.7-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-fuse_kernel.h-clean-includes.patch
    -p2 "${FILES}"/${PN}-2.8.6-gold.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    MOUNT_FUSE_PATH=/usr/$(exhost --target)/bin
    --disable-example
    --disable-static
)

src_install() {
    default

    edo rm -r "${IMAGE}"/etc
    edo rm -r "${IMAGE}"/dev

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/share/man/man8/mount.fuse.8 mount.fuse-${SLOT}.8
}

