# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="Helper tool for compiling applications and libraries"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"
DOWNLOADS="https://${PN}.freedesktop.org/releases/${PNV}.tar.gz"

CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-gnueabi
    arm-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabihf
    armv7-unknown-linux-musleabi
    armv7-unknown-linux-musleabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES=""

ECONF_SOURCE=${WORK}

src_configure() {
    local target= params=()

    for target in ${CROSS_COMPILE_TARGETS} ; do
        if option !targets:${target} ; then
            echo "    Cross-Compile Target: ${target} (disabled)"
            continue
        fi

        echo "    Cross-Compile Target: ${target}"

        edo mkdir -p "${WORKBASE}/build/${target}"
        edo cd "${WORKBASE}/build/${target}"

        params+=(
            --program-prefix=${target}-
            --with-pc-path=/usr/${target}/lib/pkgconfig:/usr/share/pkgconfig
            --with-system-include-path=/usr/${target}/include
            --with-system-library-path=/usr/${target}/lib
            --with-internal-glib
            --disable-host-tool
            --disable-indirect-deps
        )

        if ! exhost --is-native -q ; then
            # All switches below are need to cross-compile the bundled glib
            params+=(
                # "Whether the stack grows up or down; defaults to "no", which
                # works most places. If you are compiling for PA-RISC or
                # various other architectures, you'll have to change this."
                glib_cv_stack_grows=no
                # "Whether an underscore needs to be prepended to symbols when
                # looking them up via dlsym. Only needs to be set if your
                # system # uses dlopen/dlsym."
                glib_cv_uscore=yes
                # "Whether you have a getpwuid_r function (in your C library,
                # not your thread library) that conforms to the POSIX spec.
                # (Takes a 'struct passwd **' as the final argument)"
                ac_cv_func_posix_getpwuid_r=yes
                # Whether you have a getgrgid_r function that conforms to the
                # POSIX spec.
                ac_cv_func_posix_getgrgid_r=yes
            )
        fi

        econf "${params[@]}"
    done
}

src_compile() {
    local target=

    for target in ${CROSS_COMPILE_TARGETS} ; do
        if option !targets:${target} ; then
            echo "    Cross-Compile Target: ${target} (disabled)"
            continue
        fi

        echo "    Cross-Compile Target: ${target}"

        edo cd "${WORKBASE}/build/${target}"
        default
    done
}

src_install() {
    local alternatives=( pkg-config ${PN} 10 )

    local target=
    for target in ${CROSS_COMPILE_TARGETS} ; do
        if option !targets:${target} ; then
            echo "    Cross-Compile Target: ${target} (disabled)"
            continue
        fi

        echo "    Cross-Compile Target: ${target}"

        edo cd "${WORKBASE}/build/${target}"
        emake -j1 DESTDIR="${IMAGE}" install

        alternatives+=(
            /usr/$(exhost --target)/bin/${target}-pkg-config    ${target}-pkg-config.${PN}
            /usr/share/man/man1/${target}-pkg-config.1          ${target}-pkg-config.${PN}.1
        )
    done

    # install the unprefixed tool for the native target and ban it in exheres
    dosym $(exhost --target)-pkg-config.${PN}   /usr/$(exhost --target)/bin/pkg-config.${PN}
    dosym $(exhost --target)-pkg-config.${PN}.1 /usr/share/man/man1/pkg-config.${PN}.1
    dobanned pkg-config.${PN}

    alternatives+=(
        /usr/$(exhost --target)/bin/pkg-config  pkg-config.${PN}
        "${BANNEDDIR}"/pkg-config               pkg-config.${PN}

        /usr/share/aclocal/pkg.m4               pkg_${PN}.m4
        /usr/share/man/man1/pkg-config.1        pkg-config.${PN}.1
    )

    alternatives_for "${alternatives[@]}"
}

